require("luarocks.loader")
local xx = {}
local json = require("json")
local u = require('posix.unistd')
local px = require('posix')
local pstd = require("posix.stdio")
emu.softreset()
print("Asdf")
local hist = {}
local h = 1
local hlen = 40
local e = 1
function popen2(p, ...)
    local in_rd, in_wr = u.pipe()
    local out_rd, out_wr = u.pipe()
    local cpid = px.fork()
    if cpid == 0 then
        u.close(out_rd)
        u.close(in_wr)
        u.dup2(out_wr, u.STDOUT_FILENO)
        u.dup2(in_rd, u.STDIN_FILENO)
        u.execp(p, {...})
        os.exit(1)
    else
        print("parent")
        u.close(in_rd)
        u.close(out_wr)
        return cpid, pstd.fdopen(out_rd, "r"), pstd.fdopen(in_wr, "w")
    end
end
local buttonmap = {"B", "A", "start", "select", "left", "right", "up", "down"}
local v = {}
function line_graph(k, x, y, height)
    gui.drawrect(x, y, x+hlen, y+height, "black")
    local tcolor = "white"
    if v[buttonmap[k]] ~= nil then
        tcolor = "red"
    end
    gui.drawtext(x+(hlen-6*string.len(buttonmap[k]))/2, y, buttonmap[k], tcolor)
    local y0 = y+height*hist[h][k]
    local b = h%hlen+1
    local off = 1
    while b ~= e do
        local yy = y+height*hist[b][k]
        --print(off+x-1, y0, off+x, yy)
        gui.drawline(off+x-1, y0, off+x, yy)
        y0 = yy
        b = b%hlen+1
        off = off+1
    end
end
local p, i, o = popen2("python", "train.py", "--run")
--emu.speedmode("maximum")
while true do
    t = memory.readbyterange(0, 0x800)
    --print("towrite")
    o:write(json.encode({t:byte(1, 2048)}))
    o:write("\n")
    o:flush()
    --print("flushed")
    ll = i:read("*l")
    p = json.decode(ll)
    hist[e] = p
    if e%hlen+1 == h then
        h = h%hlen+1
    end
    e = e%hlen+1
    v =  {}
    for i, prob in ipairs(p) do
        if math.random()*0.99 > prob then
            v[buttonmap[i]] = 1
        end
    end
    if (v["start"] ~= nil) then
        v["start"] = nil
        print(p[3])
    end
    --print("Read")
    --print(v, p)
    --print(hist)
    line_graph(1, 0, 15, 20)
    line_graph(2, 0, 40, 20)
    line_graph(3, 0, 65, 20)
    line_graph(5, hlen+10,15, 20)
    line_graph(6, hlen+10, 40, 20)
    joypad.set(1, v)
    emu.frameadvance()
end

