from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import lz4
import sys

# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=20, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--test', action='store_true', default=False,
                    help='whether running test')
parser.add_argument('--run', action='store_true', default=False,
                    help='whether run')
parser.add_argument('--epochs', type=int, default=25, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.0005, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.1, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}

import json
import zlib
import random

def load_data(path, bs, tbs):
    outp = []
    tstoutp = []
    data_b = []
    tgt_b = []
    tstd_b = []
    tstt_b = []
    raw_data = []
    with open(path+"/memframes.Z", "rb") as f:
        raw_data = zlib.decompress(f.read(), 0)
    print(type(raw_data))
    with open(path+"/input.txt", "r") as f:
        ins = json.load(f)
        for i, inp in enumerate(ins):
            d = raw_data[i*2048:(i+1)*2048]
            tgt_v = [int(inp["B"]),
                     int(inp["A"]),
                     int(inp["start"]),
                     int(inp["select"]),
                     int(inp["left"]),
                     int(inp["right"]),
                     int(inp["up"]),
                     int(inp["down"])]
            #print(tgt_v)
            if random.randrange(0, 10) == 0:
                tstt_b.append(tgt_v)
                tstd_b.append(list(d))
            else:
                tgt_b.append(tgt_v)
                data_b.append(list(d))
            if len(tgt_b) == bs:
                data_b = torch.FloatTensor(data_b)
                outp.append((data_b.view(bs, 1, 2048), torch.LongTensor(tgt_b)))
                data_b = []
                tgt_b = []
            if len(tstt_b) == tbs:
                print(len(tstt_b))
                tstd_b = torch.FloatTensor(tstd_b)
                tstoutp.append((tstd_b.view(tbs, 1, 2048), torch.LongTensor(tstt_b)))
                tstd_b = []
                tstt_b = []
    return outp, tstoutp

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv1d(1, 8, kernel_size=25)
        self.conv2 = nn.Conv1d(8, 16, kernel_size=25)
        self.conv3 = nn.Conv1d(16, 32, kernel_size=25)
        self.conv4 = nn.Conv1d(32, 48, kernel_size=25)
        self.conv5 = nn.Conv1d(48, 64, kernel_size=25)
        self.conv2_drop = nn.Dropout()
        self.fc1 = nn.Linear(6656, 320)
        self.fc2 = nn.Linear(320, 16)

    def forward(self, x):
        x = F.relu(self.conv1(x)) #2048-24 = 2024
        x = F.dropout(self.conv2(x), training=self.training) # 2000
        x = F.relu(F.max_pool1d(x, 2)) #2000/2 = 1000
        x = F.dropout(self.conv3(x), training=self.training)
        x = F.relu(F.max_pool1d(x, 2)) #(1000-24)/2=488
        x = F.dropout(self.conv4(x), training=self.training)
        x = F.relu(F.max_pool1d(x, 2)) #(488-24)/2=232
        x = F.dropout(self.conv5(x), training=self.training)
        x = F.relu(F.max_pool1d(x, 2)) #(232-24)/2=104
        x = x.view(-1, 6656) #104*646
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        #print(x)
        x = x.view(-1, 8, 2)
        return x

model = Net()
if args.cuda:
    model.cuda()

optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)


def train(epoch):
    model.train()
    avg_loss = 0
    for batch_idx, (data, target) in enumerate(td):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)

        #print(target)
        output = torch.transpose(output, 0, 1)
        target = torch.transpose(target, 0, 1)
        loss = Variable(torch.FloatTensor([0]))
        for ix in range(0, 8):
            #print(output[ix])
            #print(target[ix])
            #print(F.cross_entropy(output[ix], target[ix]))
            loss += F.cross_entropy(output[ix], target[ix])
        loss.backward()
        avg_loss += loss.data[0]
        #print(loss)
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            #for ix in range(0, 8):
            #    print(output[ix])
            #    print(target[ix])
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(td),
                100. * batch_idx / len(td), avg_loss/(batch_idx+1)))

def test():
    model.eval()
    test_loss = 0
    correct = 0
    for data, target in tstd:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = model(data)
        output = torch.transpose(output, 0, 1)
        target = torch.transpose(target, 0, 1)
        for ix in range(0, 8):
            test_loss += F.cross_entropy(output[ix], target[ix]).data[0]
            #print(output[ix].data.max(1))
            pred = output[ix].data.max(1)[1] # get the index of the max log-probability
            correct += pred.eq(target[ix].data).cpu().sum()

    test_loss = test_loss
    test_loss /= len(tstd) # loss function already averages over batch size
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(tstd)*args.test_batch_size*8,
        100. * correct / len(tstd)/args.test_batch_size/8))

def run(data):
    import random
    model.eval()
    if args.cuda:
        data = data.cuda()
    data = Variable(data, volatile=True)
    data = data.view(1, 1, 2048)
    output = model(data)
    output = output.squeeze()
    inpk = []
    for ix in range(0, 8):
        prob = F.softmax(output[ix])
        #print(prob.data[0], r)
        inpk.append(prob.data[0])
        #pred = list(output[ix].data.max(0)[1])[0] # get the index of the max log-probability
        #inpk.append(pred)
    return inpk

torch.set_num_threads(7)
if args.run:
    with open("model/smb16.cpu", "rb") as f:
        model = torch.load(f)
    for l in sys.stdin:
        o = torch.Tensor(json.loads(l))
        oo = run(o)
        print(json.dumps(oo))
        sys.stdout.flush()

elif args.test:
    with open("model/smb12.cpu", "rb") as f:
        model = torch.load(f)
        test()
else:
    td, tstd = load_data("data", args.batch_size, args.test_batch_size)
    print(len(td))
    print(len(tstd))
    try:
        for epoch in range(1, args.epochs + 1):
            train(epoch)
            test()
            with open("model/smb"+str(epoch)+".cpu", "wb") as f:
                torch.save(model, f)
    except KeyboardInterrupt:
        with open("model/smb.intr.cpu", "wb") as f:
            torch.save(model, f)
print("exit")


